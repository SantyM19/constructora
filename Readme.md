# Ciudadela Del Futuro

La empresa “Constructores S.A.S” será la encargada de construir la “Ciudadela del Futuro”. Esta empresa requiere desarrollar una aplicación capaz de gestionar sus solicitudes de construcción en el terreno designado para la ciudadela.

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### EndPointOrigin 📋

_A este Endpoint te puedes conectar para todos los servicios_

```
https://salty-coast-69475.herokuapp.com/
```

### Servicios 🔧

##Crear Proyecto

Puedes crear un nuevo proyecto global para otra ciudadela 

```
proyectos/crear
```

Con el siguiente JSON

```
{
    "nombre":"CiudadelaDelFuturo",
    "arquitectos":[
        "61439933fccefe26af16f42a", 
        "61439943fccefe26af16f42b"
    ],
    "materialBase":"61436c62be3fe92074ee4ad6"
}
```

Existe un servicio para crear los arquitecto, si el Id del arquitecto no existe, no te deja continuar

_Podrias usar el siguiente servicio para ver los id de arquitectos_

```
/arquitectos
```

Para el Material Base se debe utilizar un id ya existente en la base de datos, normalmente es predetermianda

```
61436c62be3fe92074ee4ad6
```

_Podrias usar el siguiente servicio para ver los id de material base_

```
/base
```
Una vez creado el proyecto deberas tener en cuenta el id de este proyecto, por que de este depende el servicio que se va a crear

##Crear Solicitud

Para crear la solicitud necesitaras usar el siguiente servicio

```
/solicitudes/crear
```

Con este JSON como base

```
{
    "nombre":"Estadio Los Libertadores",
    "arquitecto":"61439933fccefe26af16f42a",
    "proyecto":"6148b8d6a704a37332034271",
    "tipo":"6143962dfccefe26af16f425",
    "coordenadaX":1,
    "coordenadaY":1
}
```

Deberas tener en cuenta que el Id del arquitecto, debe ser uno que este ligado a este proyecto Global

En proyecto deberas poner el Id de el proyecto creado anteriormente, por ejemplo el id del proyecto Ciudadela del Futuro
por si deseas observar estos proyectos

```
/proyectos
```

El tipo de construccion que necesitas, existe una para cada tipo de construccion, por si deseas chequear

```
/construcciones
```

_El siguiente link es para ver todos los Servicios_

Solo debes cambiar las Url de origin

https://www.getpostman.com/collections/a5a8ed47992666fd0a43