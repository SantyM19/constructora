package com.co.constructora.constructora.DTO;

public class ArquitectoDTO {
    private String id;
    private String nombre;

    public ArquitectoDTO(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
