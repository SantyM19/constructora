package com.co.constructora.constructora.DTO;

public class ConstruccionesDTO {
    private String id;
    private String nombre;
    private String materialBase;
    private Integer tiempoConstruccion;

    public ConstruccionesDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMaterialBase() {
        return materialBase;
    }

    public void setMaterialBase(String materialBase) {
        this.materialBase = materialBase;
    }

    public Integer getTiempoConstruccion() {
        return tiempoConstruccion;
    }

    public void setTiempoConstruccion(Integer tiempoConstruccion) {
        this.tiempoConstruccion = tiempoConstruccion;
    }
}
