package com.co.constructora.constructora.DTO;

import java.util.ArrayList;

public class MaterialBaseDTO {
    private String id;
    private String nombre;
    private ArrayList<String> materialBase;

    public MaterialBaseDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<String> getMaterialBase() {
        return materialBase;
    }

    public void setMaterialBase(ArrayList<String> materialBase) {
        this.materialBase = materialBase;
    }
}
