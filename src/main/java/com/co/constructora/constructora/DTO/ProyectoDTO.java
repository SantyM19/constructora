package com.co.constructora.constructora.DTO;

import java.util.ArrayList;

public class ProyectoDTO {
    private String id;
    private String nombre;
    private ArrayList<String> arquitectos;
    private String materialBase;
    private ArrayList<String> solicitudes;
    private Integer tiempoFinalizacion;

    public ProyectoDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<String> getArquitectos() {
        return arquitectos;
    }

    public void setArquitectos(ArrayList<String> arquitectos) {
        this.arquitectos = arquitectos;
    }

    public String getMaterialBase() {
        return materialBase;
    }

    public void setMaterialBase(String materialBase) {
        this.materialBase = materialBase;
    }

    public ArrayList<String> getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(ArrayList<String> solicitudes) {
        this.solicitudes = solicitudes;
    }

    public Integer getTiempoFinalizacion() {
        return tiempoFinalizacion;
    }

    public void setTiempoFinalizacion(Integer tiempoFinalizacion) {
        this.tiempoFinalizacion = tiempoFinalizacion;
    }
}
