package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.ArquitectoDTO;
import com.co.constructora.constructora.domain.Arquitecto;
import com.co.constructora.constructora.domain.repository.ArquitectoRepository;
import com.co.constructora.constructora.mappers.ArquitectoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArquitectoService {
    @Autowired
    ArquitectoRepository arquitectoRepository;

    ArquitectoMapper mapper = new ArquitectoMapper();

    public ArquitectoDTO crear(ArquitectoDTO arquitectoDTO){
        Arquitecto arquitecto = mapper.fromArquitectoDTOToArquitecto(arquitectoDTO);
        return mapper.fromArquitectoToArquitectoDTO(arquitectoRepository.save(arquitecto));
    }

    public List<ArquitectoDTO> obternerTodos(){
        List<Arquitecto> arquitectos = arquitectoRepository.findAll();
        return mapper.fromCollectionList(arquitectos);
    }

    public ArquitectoDTO arquitectoById(String id){
        Arquitecto arquitecto = arquitectoRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontro el arquitecto."));
        return mapper.fromArquitectoToArquitectoDTO(arquitecto);
    }

    public ArquitectoDTO arquitectoByName(String name){
        Arquitecto arquitecto = arquitectoRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró el arquitecto."));
        return mapper.fromArquitectoToArquitectoDTO(arquitecto);
    }

    public ArquitectoDTO actualizar(String id, ArquitectoDTO arquitectoDTO){

        Arquitecto arquitectoExist = arquitectoRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontro el arquitecto."));
        Arquitecto arquitecto = mapper.fromArquitectoDTOToArquitecto(arquitectoDTO);
        arquitecto.setId(arquitectoExist.getId());

        return mapper.fromArquitectoToArquitectoDTO(arquitectoRepository.save(arquitecto));
    }

    public String borrar(String id){
        arquitectoRepository.deleteById(id);
        return id + " Delete Ok";
    }

}
