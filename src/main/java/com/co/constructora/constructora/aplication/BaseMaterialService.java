package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.MaterialBaseDTO;
import com.co.constructora.constructora.domain.BaseMaterial;
import com.co.constructora.constructora.domain.Material;
import com.co.constructora.constructora.domain.repository.BaseMaterialRepository;
import com.co.constructora.constructora.domain.repository.MaterialRepository;
import com.co.constructora.constructora.infrastructure.MaterialController;
import com.co.constructora.constructora.mappers.BaseMaterialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BaseMaterialService {
    @Autowired
    BaseMaterialRepository baseMaterialRepository;

    @Autowired
    MaterialRepository materialRepository;

    BaseMaterialMapper mapper = new BaseMaterialMapper();

    public MaterialBaseDTO crear(MaterialBaseDTO materialBaseDTO){
        Optional<Material> material;
        Material cloneMaterial = new Material();
        ArrayList<String> materials = new ArrayList<>();
        BaseMaterial baseMaterial = mapper.fromMaterialBaseDTOToBaseMaterial(materialBaseDTO);

        for (String i : baseMaterial.getBaseMaterial()) {
            material = materialRepository.findById(i);

            cloneMaterial.setId(null);
            cloneMaterial.setAbbreviation(material.get().getAbbreviation());
            cloneMaterial.setAmount(material.get().getAmount());
            cloneMaterial.setName(material.get().getName());

            materials.add(materialRepository.save(cloneMaterial).getId());
        }

        baseMaterial.setBaseMaterial(materials);

        return mapper.fromBaseMaterialToMaterialBaseDTO(baseMaterialRepository.save(baseMaterial));
    }

    public List<MaterialBaseDTO> obternerTodos(){
        List<BaseMaterial> baseMaterials = baseMaterialRepository.findAll();
        return mapper.fromCollectionList(baseMaterials);
    }

    public MaterialBaseDTO materialBaseById(String id){
        BaseMaterial baseMaterial = baseMaterialRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este material base."));
        return mapper.fromBaseMaterialToMaterialBaseDTO(baseMaterial);
    }

    public MaterialBaseDTO materialBaseByName(String name){
        BaseMaterial baseMaterial = baseMaterialRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este material base."));
        return mapper.fromBaseMaterialToMaterialBaseDTO(baseMaterial);
    }

    public MaterialBaseDTO actualizar(String id, MaterialBaseDTO materialBase){

        BaseMaterial materialBExist = baseMaterialRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho material base."));
        BaseMaterial materialBDTO = mapper.fromMaterialBaseDTOToBaseMaterial(materialBase);
        materialBDTO.setId(materialBExist.getId());

        return mapper.fromBaseMaterialToMaterialBaseDTO(baseMaterialRepository.save(materialBDTO));
    }

    public String borrar(String id){
        baseMaterialRepository.deleteById(id);
        return id + " Delete Ok";
    }
}
