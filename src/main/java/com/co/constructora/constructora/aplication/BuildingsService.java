package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.ConstruccionesDTO;
import com.co.constructora.constructora.domain.BaseMaterial;
import com.co.constructora.constructora.domain.Buildings;
import com.co.constructora.constructora.domain.repository.BaseMaterialRepository;
import com.co.constructora.constructora.domain.repository.BuildingsRepository;
import com.co.constructora.constructora.mappers.BuildingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BuildingsService {
    @Autowired
    BuildingsRepository buildingsRepository;

    @Autowired
    BaseMaterialRepository baseMaterialRepository;

    BuildingsMapper mapper = new BuildingsMapper();

    public ConstruccionesDTO crear(ConstruccionesDTO construccionesDTO){
        Buildings buildings = mapper.fromConstruccionesToBuildings(construccionesDTO);
        Optional<BaseMaterial> baseMaterial = baseMaterialRepository.findById(buildings.getBaseMaterial());
        baseMaterial.get().setId(null);
        buildings.setBaseMaterial(baseMaterialRepository.save(baseMaterial.get()).getId());
        return mapper.fromBuildingsToConstrucciones(buildingsRepository.save(buildings));
    }

    public List<ConstruccionesDTO> obternerTodos(){
        List<Buildings> buildings = buildingsRepository.findAll();
        return mapper.fromCollectionList(buildings);
    }

    public ConstruccionesDTO construccionesById(String id){
        Buildings buildings = buildingsRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontro esa construccion."));
        return mapper.fromBuildingsToConstrucciones(buildings);
    }

    public ConstruccionesDTO construccionesByName(String name){
        Buildings buildings = buildingsRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró la construccion."));
        return mapper.fromBuildingsToConstrucciones(buildings);
    }

    public ConstruccionesDTO actualizar(String id, ConstruccionesDTO construccionesDTO){

        Buildings buildingsExist = buildingsRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicha construction."));
        Buildings buildings = mapper.fromConstruccionesToBuildings(construccionesDTO);
        buildings.setId(buildingsExist.getId());

        return mapper.fromBuildingsToConstrucciones(buildingsRepository.save(buildings));
    }

    public String borrar(String id){
        buildingsRepository.deleteById(id);
        return id + " Delete Ok";
    }

}
