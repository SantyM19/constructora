package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.MaterialDTO;
import com.co.constructora.constructora.domain.Material;
import com.co.constructora.constructora.domain.repository.MaterialRepository;
import com.co.constructora.constructora.mappers.MaterialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialService {
    @Autowired
    MaterialRepository materialRepository;


    MaterialMapper mapper = new MaterialMapper();

    public MaterialDTO crear(MaterialDTO materialDTO){
        Material material = mapper.fromMaterialDTOToMaterial(materialDTO);
        return mapper.fromMaterialToMaterialDTO(materialRepository.save(material));
    }

    public List<MaterialDTO> obternerTodos(){
        List<Material> material = materialRepository.findAll();
        return mapper.fromCollectionList(material);
    }

    public MaterialDTO materialById(String id){
        Material material = materialRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este material."));
        return mapper.fromMaterialToMaterialDTO(material);
    }

    public MaterialDTO materialByName(String name){
        Material material = materialRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este material."));
        return mapper.fromMaterialToMaterialDTO(material);
    }

    public List<MaterialDTO> materialByAbreviatura(String abr){
        List<Material> material = materialRepository.findByAbbreviation(abr).
                orElseThrow(()->new IllegalArgumentException("no se encontró este material."));
        return mapper.fromCollectionList(material);
    }

    public MaterialDTO actualizar(String id, MaterialDTO material){

        Material materialExist = materialRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho material."));
        Material materialDTO = mapper.fromMaterialDTOToMaterial(material);
        materialDTO.setId(material.getId());

        return mapper.fromMaterialToMaterialDTO(materialRepository.save(materialDTO));
    }

    public String borrar(String id){
        materialRepository.deleteById(id);
        return id + " Delete Ok";
    }
}
