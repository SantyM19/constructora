package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.MaterialBaseDTO;
import com.co.constructora.constructora.DTO.ProyectoDTO;
import com.co.constructora.constructora.domain.BaseMaterial;
import com.co.constructora.constructora.domain.Material;
import com.co.constructora.constructora.domain.Project;
import com.co.constructora.constructora.domain.repository.ArquitectoRepository;
import com.co.constructora.constructora.domain.repository.BaseMaterialRepository;
import com.co.constructora.constructora.domain.repository.MaterialRepository;
import com.co.constructora.constructora.domain.repository.ProjectRespository;
import com.co.constructora.constructora.mappers.BaseMaterialMapper;
import com.co.constructora.constructora.mappers.ProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ProjectService {
    @Autowired
    ProjectRespository projectRepository;

    @Autowired
    ArquitectoRepository arquitecto;

    @Autowired
    BaseMaterialRepository baseMaterialRepository;

    @Autowired
    MaterialRepository materialRepository;

    ProjectMapper mapper = new ProjectMapper();

    public Object crear(ProyectoDTO proyectoDTO){
        Project project = mapper.fromProyectoDTOToProject(proyectoDTO);

        Optional<Material> material;
        Material cloneMaterial = new Material();
        ArrayList<String> materials = new ArrayList<>();

        for (String i : project.getArchitects()) {
            if (!arquitecto.existsById(i)) return "El Arquitecto con ID " + i +" no existe";
        }

        if (!baseMaterialRepository.existsById(project.getBaseMaterial())) return "El Material Base con ID " + project.getBaseMaterial() +" no existe";

        BaseMaterial baseMaterial = baseMaterialRepository.findById(project.getBaseMaterial()).get();
        baseMaterial.setId(null);

        for (String i : baseMaterial.getBaseMaterial()) {
            material = materialRepository.findById(i);

            cloneMaterial.setId(null);
            cloneMaterial.setAbbreviation(material.get().getAbbreviation());
            cloneMaterial.setAmount(10000.);
            cloneMaterial.setName(material.get().getName());

            materials.add(materialRepository.save(cloneMaterial).getId());
        }

        baseMaterial.setBaseMaterial(materials);
        baseMaterial = baseMaterialRepository.save(baseMaterial);

        project.setBaseMaterial(baseMaterial.getId());
        project.setRequests(new ArrayList<>());

        return mapper.fromProjectToProyecto(projectRepository.save(project));
    }

    public List<ProyectoDTO> obternerTodos(){
        List<Project> project = projectRepository.findAll();
        return mapper.fromCollectionList(project);
    }

    public ProyectoDTO proyectoById(String id){
        Project project = projectRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró este proyecto."));
        return mapper.fromProjectToProyecto(project);
    }

    public ProyectoDTO proyectoByName(String name){
        Project project = projectRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró este proyecto."));
        return mapper.fromProjectToProyecto(project);
    }

    public ProyectoDTO actualizar(String id, ProyectoDTO proyecto){

        Project projectExist = projectRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicho proyecto."));
        Project project = mapper.fromProyectoDTOToProject(proyecto);
        project.setId(projectExist.getId());

        return mapper.fromProjectToProyecto(projectRepository.save(project));
    }

    public String borrar(String id){
        projectRepository.deleteById(id);
        return id + " Delete Ok";
    }
}
