package com.co.constructora.constructora.aplication;

import com.co.constructora.constructora.DTO.SolicitudDTO;
import com.co.constructora.constructora.domain.*;
import com.co.constructora.constructora.domain.repository.*;
import com.co.constructora.constructora.mappers.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestService {
    @Autowired
    RequestRepository requestRepository;

    @Autowired
    ArquitectoRepository arquitectoRepository;
    @Autowired
    ProjectRespository projectRepository;
    @Autowired
    BuildingsRepository buildingsRepository;
    @Autowired
    BaseMaterialRepository baseMaterialRepository;
    @Autowired
    MaterialRepository materialRepository;

    RequestMapper mapper = new RequestMapper();

    public Object crear(SolicitudDTO solicitudDTO){
        Request request = mapper.formSolicitudDTOToRequest(solicitudDTO);

        ArrayList<String> list = null;

        if (!arquitectoRepository.existsById(request.getArchitect())) return "El Arquitecto con ID " + request.getArchitect() +" no existe";
        if (!projectRepository.existsById(request.getProject())) return "El Proyecto con ID " + request.getProject() +" no existe";
        if (!buildingsRepository.existsById(request.getTipo())) return "El Tipo de Construccion con ID " + request.getTipo() +" no existe";

        if (requestRepository.existsByCoordinateX(request.getCoordinateX()) && requestRepository.existsByCoordinateY(request.getCoordinateY())) return "En estas Coordenadas ya existe una construction";


        Project proyecto = projectRepository.findById(request.getProject()).get();
        BaseMaterial bMaterial = baseMaterialRepository.findById(proyecto.getBaseMaterial()).get();
        ArrayList<String> baseMaterialProject = bMaterial.getBaseMaterial();


        Buildings buildings = buildingsRepository.findById(request.getTipo()).get();
        bMaterial = baseMaterialRepository.findById(buildings.getBaseMaterial()).get();
        ArrayList<String> baseMaterialBuilding = bMaterial.getBaseMaterial();

        Material materialP = new Material();
        Material materialB = new Material();


        for (String i : baseMaterialProject) {
            materialP = materialRepository.findById(i).get();
            for (String ii : baseMaterialBuilding){
                materialB = materialRepository.findById(ii).get();
                if(materialB.getAbbreviation().equals(materialP.getAbbreviation())){
                    materialP.setAmount(materialP.getAmount() - materialB.getAmount() );
                    if(materialP.getAmount() < 0) return "Este proyecto , No contiene materiales suficientes para esta solicitud " + materialP.getName();
                    materialRepository.save(materialP);
                }
            }
        }


        if(proyecto.getTimeToFinish() != null){
            System.out.println("old");
            proyecto.setTimeToFinish(proyecto.getTimeToFinish() + buildings.getTimeToBuild() );
        }

        if (proyecto.getTimeToFinish() == null){
            System.out.println("new");
            proyecto.setTimeToFinish( 1 + buildings.getTimeToBuild());
        }

        request.setState("Pendiente");
        requestRepository.save(request);

        System.out.println(request);
        System.out.println("Ok");

        proyecto.addRequests(request.getId());

        System.out.println(proyecto);

        projectRepository.save(proyecto);

        return mapper.fromRequestToSolicitud(request);
    }

    public List<SolicitudDTO> obternerTodos(){
        List<Request> request = requestRepository.findAll();
        return mapper.fromCollectionList(request);
    }

    public SolicitudDTO solicitudesById(String id){
        Request request = requestRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontro esa solicitud."));
        return mapper.fromRequestToSolicitud(request);
    }

    public SolicitudDTO solicitudesByName(String name){
        Request request = requestRepository.findByName(name).
                orElseThrow(()->new IllegalArgumentException("no se encontró la solicitud."));
        return mapper.fromRequestToSolicitud(request);
    }

    public SolicitudDTO actualizar(String id, SolicitudDTO solicitud){

        Request requestExist = requestRepository.findById(id).
                orElseThrow(()->new IllegalArgumentException("no se encontró dicha solicitud."));
        Request request = mapper.formSolicitudDTOToRequest(solicitud);
        request.setId(requestExist.getId());

        return mapper.fromRequestToSolicitud(requestRepository.save(request));
    }

    public String borrar(String id){
        requestRepository.deleteById(id);
        return id + " Delete Ok";
    }
}
