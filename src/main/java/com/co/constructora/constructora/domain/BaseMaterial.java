package com.co.constructora.constructora.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document
public class BaseMaterial {
    @Id
    private String id;
    private String name;
    private ArrayList<String> baseMaterial;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getBaseMaterial() {
        return baseMaterial;
    }

    public void setBaseMaterial(ArrayList<String> baseMaterial) {
        this.baseMaterial = baseMaterial;
    }

    @Override
    public String toString() {
        return "BaseMaterial{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", baseMaterial=" + baseMaterial +
                '}';
    }
}
