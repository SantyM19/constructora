package com.co.constructora.constructora.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document
public class Project {
    @Id
    private String id;
    private String name;
    private ArrayList<String> architects;
    private String baseMaterial;
    private ArrayList<String> requests;
    private Integer timeToFinish;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getArchitects() {
        return architects;
    }

    public void setArchitects(ArrayList<String> architects) {
        this.architects = architects;
    }

    public String getBaseMaterial() {
        return baseMaterial;
    }

    public void setBaseMaterial(String baseMaterial) {
        this.baseMaterial = baseMaterial;
    }

    public ArrayList<String> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<String> requests) {
        this.requests = requests;
    }


    public void addRequests(String requests) {
        this.requests.add(requests);
    }

    public Integer getTimeToFinish() {
        return timeToFinish;
    }

    public void setTimeToFinish(Integer timeToFinish) {
        this.timeToFinish = timeToFinish;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", architects=" + architects +
                ", baseMaterial='" + baseMaterial + '\'' +
                ", requests=" + requests +
                ", timeToFinish='" + timeToFinish + '\'' +
                '}';
    }
}
