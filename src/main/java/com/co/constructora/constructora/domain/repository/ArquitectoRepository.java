package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.Arquitecto;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ArquitectoRepository extends MongoRepository<Arquitecto, String> {
    Optional<Arquitecto> findByName(String name);
}
