package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.BaseMaterial;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BaseMaterialRepository extends MongoRepository<BaseMaterial, String> {
    Optional<BaseMaterial> findByName(String name);
}
