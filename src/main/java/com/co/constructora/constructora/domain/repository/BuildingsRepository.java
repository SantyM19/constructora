package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.Buildings;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BuildingsRepository extends MongoRepository<Buildings, String> {
    Optional<Buildings> findByName(String name);
}
