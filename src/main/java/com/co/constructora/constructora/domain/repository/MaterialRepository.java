package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.Material;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface MaterialRepository extends MongoRepository<Material, String> {
    Optional<Material> findByName(String name);

    Optional<List<Material>> findByAbbreviation(String abr);
}
