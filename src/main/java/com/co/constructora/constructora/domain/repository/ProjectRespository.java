package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProjectRespository extends MongoRepository<Project, String> {
    Optional<Project> findByName(String name);
}
