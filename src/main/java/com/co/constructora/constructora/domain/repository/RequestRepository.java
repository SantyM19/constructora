package com.co.constructora.constructora.domain.repository;

import com.co.constructora.constructora.domain.Request;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RequestRepository extends MongoRepository<Request, String> {
    Optional<Request> findByName(String name);

    boolean existsByCoordinateX(Integer coordinateX);

    boolean existsByCoordinateY(Integer coordinateY);
}
