package com.co.constructora.constructora.infrastructure;

import com.co.constructora.constructora.DTO.ArquitectoDTO;
import com.co.constructora.constructora.aplication.ArquitectoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/arquitectos")
public class ArquitectoController {
    @Autowired
    ArquitectoService arquitectoService;

    @GetMapping()
    public ResponseEntity<List<ArquitectoDTO>> findAll() {
        return new ResponseEntity(arquitectoService.obternerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<ArquitectoDTO> create(@RequestBody ArquitectoDTO arquitectoDTO) {
        return new ResponseEntity(arquitectoService.crear(arquitectoDTO), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ArquitectoDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(arquitectoService.arquitectoById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<ArquitectoDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(arquitectoService.arquitectoByName(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<ArquitectoDTO> update(@RequestBody ArquitectoDTO arquitectoDTO,@PathVariable("id") String id){
        return new ResponseEntity(arquitectoService.actualizar(id, arquitectoDTO), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(arquitectoService.borrar(id), HttpStatus.OK);
    }

}
