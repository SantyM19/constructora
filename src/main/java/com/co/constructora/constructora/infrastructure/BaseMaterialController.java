package com.co.constructora.constructora.infrastructure;

import com.co.constructora.constructora.DTO.MaterialBaseDTO;
import com.co.constructora.constructora.aplication.BaseMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/base")
public class BaseMaterialController {
    @Autowired
    BaseMaterialService service;

    @GetMapping()
    public ResponseEntity<List<MaterialBaseDTO>> findAll() {
        return new ResponseEntity(service.obternerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<MaterialBaseDTO> create(@RequestBody MaterialBaseDTO materialB) {
        return new ResponseEntity(service.crear(materialB), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<MaterialBaseDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.materialBaseById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<MaterialBaseDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.materialBaseByName(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<MaterialBaseDTO> update(@RequestBody MaterialBaseDTO materialB,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, materialB), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
