package com.co.constructora.constructora.infrastructure;

import com.co.constructora.constructora.DTO.ConstruccionesDTO;
import com.co.constructora.constructora.aplication.BuildingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/construcciones")
public class BuildingController {
    @Autowired
    BuildingsService service;

    @GetMapping()
    public ResponseEntity<List<ConstruccionesDTO>> findAll() {
        return new ResponseEntity(service.obternerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<ConstruccionesDTO> create(@RequestBody ConstruccionesDTO construcciones) {
        return new ResponseEntity(service.crear(construcciones), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ConstruccionesDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.construccionesById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<ConstruccionesDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.construccionesByName(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<ConstruccionesDTO> update(@RequestBody ConstruccionesDTO construcciones,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, construcciones), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
