package com.co.constructora.constructora.infrastructure;

import com.co.constructora.constructora.DTO.MaterialDTO;
import com.co.constructora.constructora.aplication.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/material")
public class MaterialController {
    @Autowired
    MaterialService service;

    @GetMapping()
    public ResponseEntity<List<MaterialDTO>> findAll() {
        return new ResponseEntity(service.obternerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<MaterialDTO> create(@RequestBody MaterialDTO material) {
        return new ResponseEntity(service.crear(material), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<MaterialDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(service.materialById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<MaterialDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(service.materialByName(name), HttpStatus.OK);
    }

    @GetMapping("/abreviatura/{abr}")
    public ResponseEntity<MaterialDTO> findByAbreviture(@PathVariable("abr") String abr){
        return new ResponseEntity(service.materialByAbreviatura(abr), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<MaterialDTO> update(@RequestBody MaterialDTO material,@PathVariable("id") String id){
        return new ResponseEntity(service.actualizar(id, material), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(service.borrar(id), HttpStatus.OK);
    }
}
