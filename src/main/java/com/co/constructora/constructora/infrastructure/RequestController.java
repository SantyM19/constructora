package com.co.constructora.constructora.infrastructure;

import com.co.constructora.constructora.DTO.ArquitectoDTO;
import com.co.constructora.constructora.DTO.SolicitudDTO;
import com.co.constructora.constructora.aplication.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/solicitudes")
public class RequestController {
    @Autowired
    RequestService requestService;

    @GetMapping()
    public ResponseEntity<List<SolicitudDTO>> findAll() {
        return new ResponseEntity(requestService.obternerTodos(), HttpStatus.OK);
    }

    @PostMapping("/crear")
    public ResponseEntity<SolicitudDTO> create(@RequestBody SolicitudDTO solicitudDTO) {
        return new ResponseEntity(requestService.crear(solicitudDTO), HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<SolicitudDTO> findById(@PathVariable("id") String id){
        return new ResponseEntity(requestService.solicitudesById(id), HttpStatus.OK);
    }

    @GetMapping("/nombre/{name}")
    public ResponseEntity<SolicitudDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity(requestService.solicitudesByName(name), HttpStatus.OK);
    }

    @PatchMapping("/actualizar/{id}")
    public ResponseEntity<SolicitudDTO> update(@RequestBody SolicitudDTO solicitud,@PathVariable("id") String id){
        return new ResponseEntity(requestService.actualizar(id, solicitud), HttpStatus.OK);
    }

    @DeleteMapping("/borrar/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        return new ResponseEntity(requestService.borrar(id), HttpStatus.OK);
    }
}
