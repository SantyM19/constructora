package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.ArquitectoDTO;
import com.co.constructora.constructora.domain.Arquitecto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ArquitectoMapper {

    public Arquitecto fromArquitectoDTOToArquitecto(ArquitectoDTO arquitectoDTO){
        Arquitecto arquitecto = new Arquitecto();
        arquitecto.setId(arquitectoDTO.getId());
        arquitecto.setName(arquitectoDTO.getNombre());

        return arquitecto;
    }

    public ArquitectoDTO fromArquitectoToArquitectoDTO(Arquitecto arquitecto){
        ArquitectoDTO arquitectoDTO = new ArquitectoDTO();
        arquitectoDTO.setId(arquitecto.getId());
        arquitectoDTO.setNombre(arquitecto.getName());

        return arquitectoDTO;
    }

    public List<ArquitectoDTO> fromCollectionList(List<Arquitecto> collection) {
        if (collection == null) {
            return null;

        }
        List<ArquitectoDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()) {
            Arquitecto arquitecto = (Arquitecto) listTracks.next();
            list.add(fromArquitectoToArquitectoDTO(arquitecto));
        }

        return list;
    }
}
