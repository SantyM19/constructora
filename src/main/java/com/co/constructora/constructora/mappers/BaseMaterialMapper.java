package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.MaterialBaseDTO;
import com.co.constructora.constructora.domain.BaseMaterial;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BaseMaterialMapper {
    public BaseMaterial fromMaterialBaseDTOToBaseMaterial(MaterialBaseDTO materialBaseDTO){
        BaseMaterial baseMaterial = new BaseMaterial();
        baseMaterial.setBaseMaterial(materialBaseDTO.getMaterialBase());
        baseMaterial.setId(materialBaseDTO.getId());
        baseMaterial.setName(materialBaseDTO.getNombre());

        return baseMaterial;

    }

    public MaterialBaseDTO fromBaseMaterialToMaterialBaseDTO(BaseMaterial baseMaterial){
        MaterialBaseDTO materialBaseDTO = new MaterialBaseDTO();
        materialBaseDTO.setId(baseMaterial.getId());
        materialBaseDTO.setMaterialBase(baseMaterial.getBaseMaterial());
        materialBaseDTO.setNombre(baseMaterial.getName());

        return materialBaseDTO;

    }

    public List<MaterialBaseDTO> fromCollectionList(List<BaseMaterial> collection){
        if (collection == null) {
            return null;
        }

        List<MaterialBaseDTO> list = new ArrayList<>(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            BaseMaterial baseMaterial = (BaseMaterial) listTracks.next();
            list.add(fromBaseMaterialToMaterialBaseDTO(baseMaterial));
        }

        return list;

    }

}
