package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.ConstruccionesDTO;
import com.co.constructora.constructora.domain.Buildings;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class BuildingsMapper {
    public Buildings fromConstruccionesToBuildings(ConstruccionesDTO construcciones){
        Buildings buildings = new Buildings();
        buildings.setBaseMaterial(construcciones.getMaterialBase());
        buildings.setId(construcciones.getId());
        buildings.setName(construcciones.getNombre());
        buildings.setTimeToBuild(construcciones.getTiempoConstruccion());

        return buildings;
    }

    public ConstruccionesDTO fromBuildingsToConstrucciones (Buildings buildings){
        ConstruccionesDTO construcciones = new ConstruccionesDTO();
        construcciones.setId(buildings.getId());
        construcciones.setMaterialBase(buildings.getBaseMaterial());
        construcciones.setNombre(buildings.getName());
        construcciones.setTiempoConstruccion(buildings.getTimeToBuild());

        return construcciones;

    }

    public List<ConstruccionesDTO> fromCollectionList(List<Buildings> collection){
        if (collection == null){
            return null;
        }

        List<ConstruccionesDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Buildings buildings = (Buildings) listTracks.next();
            list.add(fromBuildingsToConstrucciones(buildings));
        }

        return list;

    }

}
