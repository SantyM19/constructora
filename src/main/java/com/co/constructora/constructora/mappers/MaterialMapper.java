package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.MaterialDTO;
import com.co.constructora.constructora.domain.Material;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class MaterialMapper {
    public Material fromMaterialDTOToMaterial(MaterialDTO materialDTO){
        Material material = new Material();
        material.setId(materialDTO.getId());
        material.setAbbreviation(materialDTO.getAbreviacion());
        material.setName(materialDTO.getNombre());
        material.setAmount(materialDTO.getCantidad());

        return material;
    }

    public MaterialDTO fromMaterialToMaterialDTO(Material material){
        MaterialDTO materialDTO = new MaterialDTO();
        materialDTO.setAbreviacion(material.getAbbreviation());
        materialDTO.setCantidad(material.getAmount());
        materialDTO.setId(material.getId());
        materialDTO.setNombre(material.getName());

        return materialDTO;

    }

    public List<MaterialDTO> fromCollectionList(List<Material> collection){
        if (collection == null){
            return null;
        }

        List<MaterialDTO> list = new ArrayList<>(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Material material = (Material) listTracks.next();
            list.add(fromMaterialToMaterialDTO(material));
        }

        return list;

    }

}
