package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.ProyectoDTO;
import com.co.constructora.constructora.domain.Project;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ProjectMapper {
    public Project fromProyectoDTOToProject(ProyectoDTO proyecto){
        Project project = new Project();
        project.setArchitects(proyecto.getArquitectos());
        project.setBaseMaterial(proyecto.getMaterialBase());
        project.setId(proyecto.getId());
        project.setName(proyecto.getNombre());
        project.setRequests(proyecto.getSolicitudes());
        project.setTimeToFinish(proyecto.getTiempoFinalizacion());

        return project;
    }

    public ProyectoDTO fromProjectToProyecto (Project project){
        ProyectoDTO proyecto = new ProyectoDTO();
        proyecto.setArquitectos(project.getArchitects());
        proyecto.setId(project.getId());
        proyecto.setMaterialBase(project.getBaseMaterial());
        proyecto.setNombre(project.getName());
        proyecto.setSolicitudes(project.getRequests());
        proyecto.setTiempoFinalizacion(project.getTimeToFinish());

        return proyecto;

    }

    public List<ProyectoDTO> fromCollectionList(List<Project> collection){
        if(collection == null){
            return null;
        }

        List<ProyectoDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()){
            Project project = (Project) listTracks.next();
            list.add(fromProjectToProyecto(project));
        }

        return list;

    }

}
