package com.co.constructora.constructora.mappers;

import com.co.constructora.constructora.DTO.SolicitudDTO;
import com.co.constructora.constructora.domain.Request;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class RequestMapper {
    public SolicitudDTO fromRequestToSolicitud (Request request){
        SolicitudDTO solicitudDTO = new SolicitudDTO();
        solicitudDTO.setId(request.getId());
        solicitudDTO.setNombre(request.getName());
        solicitudDTO.setProyecto(request.getProject());
        solicitudDTO.setTipo(request.getTipo());
        solicitudDTO.setArquitecto(request.getArchitect());
        solicitudDTO.setCoordenadaX(request.getCoordinateX());
        solicitudDTO.setCoordenadaY(request.getCoordinateY());
        solicitudDTO.setEstado(request.getState());

        return solicitudDTO;
    }

    public Request formSolicitudDTOToRequest (SolicitudDTO solicitud){
        Request request = new Request();
        request.setArchitect(solicitud.getArquitecto());
        request.setCoordinateX(solicitud.getCoordenadaX());
        request.setCoordinateY(solicitud.getCoordenadaY());
        request.setId(solicitud.getId());
        request.setName(solicitud.getNombre());
        request.setProject(solicitud.getProyecto());
        request.setState(solicitud.getEstado());
        request.setTipo(solicitud.getTipo());

        return request;

    }

    public List<SolicitudDTO> fromCollectionList(List<Request> collection) {
        if (collection == null) {
            return null;
        }
        List<SolicitudDTO> list = new ArrayList(collection.size());
        Iterator listTracks = collection.iterator();

        while(listTracks.hasNext()) {
            Request request = (Request) listTracks.next();
            list.add(fromRequestToSolicitud(request));
        }

        return list;
    }

}
